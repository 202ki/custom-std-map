#include <iostream>
#include <string>
#include <utility>
#include <numeric>
// #include "tree.h"
#include <my/map.h>
#include <gtest/gtest.h>
#include <map>

namespace ctest{
struct Test
{
    int number;

    ~Test()
    {
        std::cout << "Test::~Test(): " << number << std::endl;
    }
    Test()
    {
        std::cout << "Test::Test()" << std::endl;
    }
    Test(int const n)
        : number(n)
    {
        std::cout << "Test::Test(int): " << number << std::endl;
    }
    Test(Test const &other)
        : number(other.number)
    {
        std::cout << "Test::Test(Test const &) : " << number << std::endl;
    }
    Test(Test      &&other) noexcept
        : number(other.number) 
    {
        other.number = 0;
        std::cout << "Test::Test(Test &&) : " << number << std::endl;
    }
    bool operator==(Test const &t) const noexcept{
        return this->number == t.number;
    }
    Test &operator=(Test const &other)
    {
        std::cout << "Test &Test::operator=(Test const &) : " << number << " -> " << other.number << std::endl;
        if(this != &other)
        {
            this->~Test();
            new (this) Test(other);
        }
        return *this;
    }
    Test &operator=(Test &&other) noexcept
    {
        std::cout << "Test &Test::operator=(Test &&) : " << number << " -> " << other.number << std::endl;
        if(this != &other)
        {
            this->~Test();
            new (this) Test(static_cast<Test &&>(other));
        }
        return *this;
    }
};


struct lesstest
{
    bool operator()(Test const &lhs, Test const &rhs) const noexcept
    {
        return lhs.number < rhs.number;
    }
};
}

// bool operator==(ctest::Test const &t1, ctest::Test const &t2) const {
//     return t1.number == t2.number;
// }
// using namespace yeet;

yeet::map<int, char> map20(int offset = 0){
  // yeet::pair<int, char> *arr = new yeet::pair<int, char>[20];
  yeet::pair<int, char> *arr = static_cast<yeet::pair<int, char> *>(std::malloc(20 * sizeof(yeet::pair<int, char>)));
  for (int i = 0; i < 20; ++i)
  {
    char c = static_cast<char>(97 + i);
    arr[i] = {i + offset, c};
  }
  yeet::map<int, char> m1(arr, arr + 20);
  std::free(arr);
  return m1;
}

// yeet::map<int, std::vector<int>> map20_vector(int offset = 0){
//   // yeet::pair<int, char> *arr = new yeet::pair<int, char>[20];
//   yeet::pair<int, std::std::vector<int>> *arr = static_cast<yeet::pair<int, std:;std::vector<int>> *>(std::malloc(20 * sizeof(yeet::pair<int, std::std::vector<int>>)));
//   std::std::vector<int> v = {0};
//   for (int i = 0; i < 20; ++i)
//   {
//     arr[i] = {i + offset, v};
//   }
//   yeet::map<int, char> m1(arr, arr + 20);
//   std::free(arr);
//   return m1;
// }


TEST(map, string) {

  yeet::map<int, yeet::map<int, char>> super_map;
  yeet::map<int, char> m = map20();
  // std::string str = "abc";
    for (int i = 0; i < 30; ++i)
    {
      yeet::pair<int, yeet::map<int, char>> super_pair = {i, m};
      super_map.insert(super_pair);
    }
    for (int i = 0; i < 30; ++i)
    {
        EXPECT_EQ(super_map.at(i) == m, 1);
    }
}

TEST(map, teststruct){
  yeet::map<ctest::Test, ctest::Test, ctest::lesstest> custom(ctest::lesstest{});
  std::map<ctest::Test, ctest::Test, ctest::lesstest> standard(ctest::lesstest{});
  const ctest::Test test(-2);
  ctest::Test testv(40);
  yeet::pair<ctest::Test, ctest::Test> p = {test, testv};
  std::pair<ctest::Test, ctest::Test> cp = {test, testv};
  // custom.insert(p);
  std::cout << "custom" << std::endl;
  custom.emplace(test, testv);
  std::cout << "standard" << std::endl;
  standard.emplace(test, testv);
  std::cout << "done" << std::endl;
}

// TEST(map, createtree){
//     // yeet::map<ctest::Test, ctest::Test, ctest::lesstest> custom(ctest::lesstest{});
//     // std::map<ctest::Test, ctest::Test, ctest::lesstest> standard(ctest::lesstest{});
//     // const ctest::Test *testk = new ctest::Test[30];
//     // ctest::Test *testv = new ctest::Test[30];
//     int len = 30;
//     yeet::pair<ctest::Test, ctest::Test> *p = new yeet::pair<ctest::Test, ctest::Test>[len];
//     std::pair<ctest::Test, ctest::Test> *cp = new std::pair<ctest::Test, ctest::Test>[len];
//     for (int i = 0; i < len; ++i)
//     {
//         // p[i] = {ctest::Test(i), ctest::Test(-i)};
//         // cp[i] = {ctest::Test(i), ctest::Test(-i)};
//         new (p + i) yeet::pair<ctest::Test, ctest::Test>(ctest::Test(i), ctest::Test(-i));
//         new (cp + i) std::pair<ctest::Test, ctest::Test>(ctest::Test(i), ctest::Test(-i));
//     } 
//     delete[] p;
//     delete[] cp;
//     std::cout << "custom" << std::endl;
//     yeet::map<ctest::Test, ctest::Test, ctest::lesstest> custom(p, p + len, ctest::lesstest{});
//     std::cout << "standard" << std::endl;
//     std::map<ctest::Test, ctest::Test, ctest::lesstest> standard(cp, cp + len, ctest::lesstest{});
//     std::cout << "done" << std::endl;
//     // std::cout << "custom" << std::endl;
//     // custom.erase(4);
//     // std::cout << "standard" << std::endl;
//     // standard.erase(4);
//     // std::cout << "done" << std::endl;
// }



TEST(map, iterator) {
  yeet::map<int, char> m1 = map20();
  std::cout << m1.data << std::endl;

  yeet::map<int, char>::iterator it = m1.begin();
  // it++;
  for (int i = 0; i < 20; ++i)
  {
    // std::cout << *it << " " << std::endl;
    EXPECT_EQ(it->k, i);
    it++;
  }
  it--;

  for (int i = 0; i < 20; ++i)
  {
    // std::cout << *it << " " << std::endl;
    EXPECT_EQ(it->k, 19 - i);
    it--;
  }
}

TEST(map, copy_move_constructor){
  yeet::map<int, char> m1 = map20();
  yeet::map<int, char> m2(m1);
  // std::cout << m2.data << std::endl;
  yeet::map<int, char>::iterator it1 = m1.begin();
  yeet::map<int, char>::iterator it2 = m2.begin();
  // it1++;
  // it2++;
  for (int i = 0; i < 20; ++i)
  {
    // std::cout << *it2 << " " << std::endl;
    EXPECT_EQ(it1->k, it2->k);
    EXPECT_EQ(it1->v, it2->v);
    it1++;
    it2++;
  }
  it2 = m2.begin();
  yeet::map<int, char> m3(static_cast<yeet::map<int, char> &&>(m1));
  // std::cout << m3.data << std::endl;
  EXPECT_EQ(m1.data, nullptr);
  yeet::map<int, char>::iterator it3 = m3.begin();
  for (int i = 0; i < 20; ++i)
  {
    // std::cout << *it2 << " " << std::endl;
    EXPECT_EQ(it3->k, it2->k);
    EXPECT_EQ(it3->v, it2->v);
    it3++;
    it2++;
  }
}

TEST(map, copy_move_operator){
  yeet::map<int, char> m1 = map20();

  yeet::map<int, char> m2;
  m2 = m1;
  yeet::map<int, char> m3;
  m3 = static_cast<yeet::map<int, char> &&>(m1);
  // std::cout << m1.data << std::endl;
  // std::cout << m2.data << std::endl;
  // std::cout << m3.data << std::endl;
  EXPECT_EQ(m1.data, nullptr);
}

TEST(map, at){
  yeet::map<int, char> m1 = map20();
  for (int i = 0; i < 20; ++i)
  {
    char val = m1.at(i);
    const char cval = m1.at(i);
    char c = static_cast<char>(97 + i);
    EXPECT_EQ(val, c);
    EXPECT_EQ(cval, c);
  }
}

TEST(map, square_brackets){
  yeet::map<int, char> m1 = map20();
  for (int i = 0; i < 20; ++i)
  {
    char val = m1[i];
    const char cval = m1[i];
    char c = static_cast<char>(97 + i);
    EXPECT_EQ(val, c);
    EXPECT_EQ(cval, c);
  }
  m1[20];
}

TEST(map, extract){
  yeet::map<int, char> m1 = map20();
  // const yeet::map<int, char>::const_iterator it = m1.begin();
  tree<int, char> eee = m1.extract(4);
  EXPECT_EQ(eee->value.v, 'e');
  m1[4];
  m1.erase(4);
  m1.insert(eee);
  // std::cout << m1.insert(eee).v << std::endl;
  // std::free(eee);
}

TEST(map, merge){
  yeet::map<int, char> m1 = map20();
  yeet::map<int, char> m2 = map20(15);
  // std::cout << m2.data << std::endl;
  m1.merge(m2);
  // std::cout << m2.data << std::endl;
  // std::cout << m1.data << std::endl;
  yeet::map<int, char>::iterator it = m1.begin();
  for (unsigned int i = 0; i < m1.size(); ++i)
  {
    EXPECT_EQ(it->k, i);
    it++;
  }
}

TEST(map, count){
  yeet::map<int, char> m1 = map20();
  yeet::map<int, char> m2 = map20(20);
  m1.merge(m2);
  EXPECT_EQ(m1.count(3), 1);
  EXPECT_EQ(m1.count(50), 0);
}

TEST(map, find){
  yeet::map<int, char> m1 = map20();
  EXPECT_EQ((m1.find(3)++)->k, 4);
}

TEST(map, contains){
  yeet::map<int, char> m1 = map20();
  EXPECT_EQ(m1.contains(4), 1);
  EXPECT_EQ(m1.contains(50), 0);
}

TEST(map, lower_upper){
  yeet::map<int, char> m1 = map20();
  for (unsigned int i = 0; i < m1.size() - 1; ++i)
  {
    // std::cout << i << std::endl;
    // std::cout << m1.data << std::endl;
    // std::cout << i << std::endl;
    tree<int, char> t = m1.extract(static_cast<int>(i));
    // std::cout << i << std::endl;
    // std::cout << m1.data << std::endl;
    EXPECT_EQ(m1.lower_bound(static_cast<int>(i))->k, i + 1);
    // std::cout << i << std::endl;
    m1.insert(t);
    // std::cout << i << std::endl;
  }
  // m1.erase(k);
  // std::cout << m1.data << std::endl;
  // std::cout << *m1.lower_bound(k) << std::endl;
}

TEST(map, constmap){
    yeet::pair<int, char> *arr = static_cast<yeet::pair<int, char> *>(std::malloc(20 * sizeof(yeet::pair<int, char>)));
    for (int i = 0; i < 20; ++i)
    {
        char c = static_cast<char>(97 + i);
        arr[i] = {i, c};
    }
    const yeet::map<int, char> m1(arr, arr + 20);
    std::free(arr);
    EXPECT_EQ(m1.at(3), 'd');
}

int main(int argc, char* argv[]) {

  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();


    // const int ar[] = {1, 2, 3, 4, 6, 7, 8, 9};
    //
    // yeet::pair<int, char> p1 = {1, 'a'};
    // yeet::pair<int, char> p2 = {2, 'b'};
    // yeet::pair<int, char> p0 = {3};
    //
    // yeet::map<int, char> m;
    //
    // yeet::pair<int, char> arr[] = {p1, p2, {3, 'c'}, {4, 'd'}};
    //
    // yeet::map<int, char> m1(arr, arr+4);
    // yeet::map<int, char> m2(m1);

    // std::cout << m1.at(1) << m2.at(1);
    // int a = 5;
    // std::cout << m1[5] << std::endl;
    // m1.insert({5, 'f'}, {6, 'e'});
    // std::cout << m1.insert({5, 'e'}).v << std::endl;

    // std::cout << m.size() << std::endl;
    // std::cout << m1[4] << std::endl;

    // yeet::map<int, char>::iterator it = {treeit<yeet::pair<int, char>>{m1.data}};
    // yeet::map<int, char>::iterator it = m1.end();
    // std::cout << m1.size() << std::endl;
    // std::cout << m2.size() << std::endl;
    // std::cout << m1.begin()->k << " " << (m1.end())->k << std::endl;



    // tree<int> t = createtree<int>(ar, ar + 8);
    // std::cout << t << std::endl;
    // treeit<int> it = {t};
    // // it++;
    // // it++;
    // // t = balanceup(remove(it.base));
    // tree<int> n = clone(t->right);
    // std::cout << n << std::endl;
    // for (int i = 0; i < 3; ++i)
    // {
    //     std::cout << it.base->value << " ";
    //     it++;
    // }
    // std::cout << std::endl;
    // for (int i = 0; i < 7; ++i)
    // {
    //     std::cout << it.base->value << " ";
    //     it--;
    // }
    // std::cout << std::endl;
    // for (int i = 0; i < 8; ++i)
    // {
    //     std::cout << it.base->value << " ";
    //     it++;
    // }
    // treeit<int> it = &t;
    // std::cout << *it << std::endl;
    // insert(5, t);
    // t = balanceup(insert(7, t));
    // std::cout << t << std::endl;
    // t->left = rightrot(t->left);
    // std::cout << t << std::endl;
}
