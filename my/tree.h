#include <iostream>
#include "custom_pair.h"
#include "algorithm"

template<typename key, typename val>
	struct node{
		yeet::pair<key, val> value;
		node *parent;
		node *left;
		node *right;
		unsigned int height;
};
template<typename key, typename val>
using tree = node<key, val> *;

template<typename key, typename val>
void destroyTree(tree<key, val> tree){
	if(tree != nullptr){
		destroyTree(tree->left);
		destroyTree(tree->right);
		tree->~node();
		std::free(tree);
	}
}

template<typename key, typename val>
void updateheight(tree<key, val> t, bool recursive = false){
	if(t == nullptr)
		return;
	t->height = std::max(t->left == nullptr ? 0 : t->left->height + 1, t->right == nullptr ? 0 : t->right->height + 1);
	if(recursive && t->parent != nullptr)
		updateheight(t->parent, true);
}

template<typename key, typename val>
tree<key, val> leftrot(tree<key, val> t1){
	tree<key, val> t2 = t1->right;
	tree<key, val> t3 = t2->right;
	t1->right = t2->left;
	if(t1->right != nullptr)
		t1->right->parent = t1;
	t2->parent = t1->parent;
	t1->parent = t2;
	t2->left = t1;
	if(t2->parent != nullptr && t2->parent->right == t1){
		t2->parent->right = t2;
	}else if(t2->parent != nullptr){
		t2->parent->left = t2;
	}
	updateheight(t1);
	updateheight(t3);
	updateheight(t2, true);
	return t2;
}

template<typename key, typename val>
tree<key, val> rightrot(tree<key, val> t1){
	tree<key, val> t2 = t1->left;
	tree<key, val> t3 = t2->left;
	t1->left = t2->right;
	if(t1->left != nullptr)
		t1->left->parent = t1;
	t2->parent = t1->parent;
	t1->parent = t2;
	t2->right = t1;
	if(t2->parent != nullptr && t2->parent->left == t1){
		t2->parent->left = t2;
	}else if(t2->parent != nullptr){
		t2->parent->right = t2;
	}
	updateheight(t1);
	updateheight(t3);
	updateheight(t2, true);
	return t2;
}

template<typename key, typename val, class... Args>
tree<key, val> detachednode(Args&&... args){
	tree<key, val> t = static_cast<tree<key, val>>(std::malloc(sizeof(node<key, val>)));
	new (&(t->value)) yeet::pair<key, val>(static_cast<Args&&>(args)...);
	t->left = nullptr;
	t->right = nullptr;
	t->parent = nullptr;
	t->height = 0;
	return t;
}

template<typename key, typename val, typename cmp = std::less<key>>
yeet::pair<tree<key, val>, tree<key, val>> insert_tree(tree<key, val> in, tree<key, val> t, const cmp &comp = std::less<key>{}){
	if(t == nullptr){
		t = in;
		return {in, in};
	}
	if(comp(in->value.k, t->value.k)){
		if(t->left == nullptr){
			t->left = in;
			in->parent = t;
			updateheight(in, true);
			return {in, in};
		}
		return insert_tree(in, t->left, comp);
	}else{
		if(t->right == nullptr){
			t->right = in;
			in->parent = t;
			updateheight(in, true);
			return {in, in};
		}
		return insert_tree(in, t->right, comp);
	}
}

template<typename key, typename val, typename cmp = std::less<key>>
yeet::pair<tree<key, val>, tree<key, val>> insert_tree(const key &k, const val &v, tree<key, val> t, const cmp &comp = std::less<key>{}){
	tree<key, val> newtree(detachednode<key, val>(k, v));
	return insert_tree(newtree, t, comp);
}

template<typename key, typename val>
struct treeit{
	tree<key, val> base;
};

template<typename key, typename val>
tree<key, val> operator*(treeit<key, val> it){
	return it.base;
}

template<typename key, typename val>
treeit<key, val> maxup(treeit<key, val> it, bool fwd){
	if((fwd ? it.base->parent->left : it.base->parent->right) == it.base){
		it.base = it.base->parent;
	}else{
		it.base = it.base->parent;
		if(it.base->parent != nullptr)
			it = maxup(it, fwd);
	}
	return it;
}

template<typename key, typename val>
treeit<key, val> maxdown(treeit<key, val> it, bool fwd, bool left = false){
	if(!left && (fwd ? it.base->right : it.base->left) != nullptr)
		it.base = fwd ? it.base->right : it.base->left;
	if((fwd ? it.base->left : it.base->right) != nullptr){
		it.base = fwd ? it.base->left : it.base->right;
		if((fwd ? it.base->left : it.base->right) != nullptr)
			it = maxdown(it, fwd, true);
	}
	return it;
}

template<typename key, typename val>
void operator++(treeit<key, val> &it, int){
	if(it.base->right != nullptr){
		it = maxdown(it, true);
	}else{
		it = maxup(it, true);
	}
}

template<typename key, typename val>
void operator--(treeit<key, val> &it, int){
	if(it.base->left != nullptr){
		it = maxdown(it, false);
	}else{
		it = maxup(it, false);
	}
}

template<typename key, typename val>
treeit<key, val> decr(treeit<key, val> it){
	it--;
	return it;
}

template<typename key, typename val, typename cmp = std::less<key>>
yeet::pair<tree<key, val>, tree<key, val>> remove(tree<key, val> t, bool destroy = true){
	if(t->left == nullptr && t->right == nullptr){
		if(t->parent != nullptr && t->parent->left == t)
			t->parent->left = nullptr;
		else if(t->parent != nullptr)
			t->parent->right = nullptr;
		tree<key, val> p = t->parent;
		t->parent = nullptr;
		if(destroy)
			destroyTree(t);
		updateheight(p, true);
		return {p, destroy ? nullptr : t};
	}
	if(t->left == nullptr){
		t = leftrot(t);
		updateheight(t->left, true);
		return remove(t->left, destroy);
	}
	if(t->right == nullptr){
		t = rightrot(t);
		updateheight(t->right, true);
		return remove(t->right, destroy);
	}
	if(t->left->height < t->right->height){
		t = leftrot(t);
		updateheight(t->left, true);
		return remove(t->left, destroy);
	}else{
		t = rightrot(t);
		updateheight(t->right, true);
		return remove(t->right, destroy);
	}
}

template<typename key, typename val>
int deltah(tree<key, val> t){
	if(t->left == nullptr && t->right == nullptr)
		return 0;
	if(t->left == nullptr)
		return static_cast<int>(t->right->height) + 1;
	if(t->right == nullptr)
		return -static_cast<int>(t->left->height) - 1;
	int lh = static_cast<int>(t->left->height);
	int rh = static_cast<int>(t->right->height);
	return rh - lh;
}

template<typename key, typename val>
tree<key, val> balanceup(tree<key, val> t){
	if(t == nullptr)
		return nullptr;
	// std::cout << t << std::endl;
	if(deltah(t) > 1 || (deltah(t) > 0 && t->parent != nullptr && t->parent->left == t))
		t = leftrot(t);
	if(deltah(t) < -1 || (deltah(t) < 0 && t->parent != nullptr && t->parent->right == t))
		t = rightrot(t);
	if(t->parent != nullptr)
		t = balanceup(t->parent);
	return t;
}

template<typename key, typename val, typename cmp = std::less<key>>
tree<key, val> find_node(const key &k, tree<key, val> t, const cmp &comp = std::less<key>{}, int mode = 0){
	if(t == nullptr)
		return nullptr;
	if(!comp(k, t->value.k) && !comp(t->value.k, k) && (mode == 0 || mode == 1))
		return t;
	if(comp(k, t->value.k)){
		if(mode == 0)
			return find_node(k, t->left, comp);
		tree<key, val> tmp = find_node(k, t->left, comp, 1);
		if(tmp == nullptr)
			return t;
		return tmp;
	}
	if(!comp(k, t->value.k)){
		if(mode == 0)
			return find_node(k, t->right, comp, 0);
		return find_node(k, t->right, comp, 1);
	}
	return nullptr;
}

template<typename key, typename val>
tree<key, val> clone(tree<key, val> t){
	tree<key, val> out = detachednode<key, val>(t->value);
	// out->parent = t->parent;
	out->parent = nullptr;
	if(t->left != nullptr){
		tree<key, val> l = clone(t->left);
		out->left = l;
		out->left->parent = out;
	}
	if(t->right != nullptr){
		tree<key, val> r = clone(t->right);
		out->right = r;
		out->right->parent = out;
	}
	return out;
}

template<typename key, typename val>
std::ostream &printTree(std::ostream &out, tree<key, val> const tree, bool right, std::string const &prefix)
{
    if(tree != nullptr)
    {
        out << prefix << (right ? "└──" : "├──" ) << tree->value << " (" << tree->height << ")" << std::endl;
        printTree(out, tree->right, false, prefix + (right ? "    " : "│   "));
        printTree(out, tree->left, true, prefix + (right ? "    " : "│   "));
    }
    return out;
}

template<typename key, typename val>
std::ostream &operator<<(std::ostream &out, tree<key, val> const tree)
{
    return printTree(out, tree, true, "");
}
