#pragma once
// #include "map.h"
#include "tree.h"
#include <functional>
// #include "custom_pair.h"
#include <optional>

namespace yeet{
	template<typename key, typename val, typename cmp = std::less<key>>
	struct map{
		tree<key, val> data;
		cmp cmpr;
	public:
		// struct iterator{
		// 	treeit<key, val> iter;
		// 	map<key, val, cmp> *m;
		// public:
			// iterator operator+(unsigned int n);
			// iterator operator-(unsigned int n);
			// void operator++(int);
			// void operator--(int);
			// yeet::pair<key, val> &operator*() const;
			// yeet::pair<key, val> *operator->() const;
			// bool operator==(iterator it);
			// bool operator!=(iterator it);
			// std::size_t operator-(iterator it);
		// };
		// struct const_iterator{
		// 	treeit<key, val> iter;
		// 	const map<key, val, cmp> *m;
		// 	void operator++(int);
		// 	void operator--(int);
		// public:
		// 	const_iterator operator+(unsigned int n);
		// 	const_iterator operator-(unsigned int n);
		// 	yeet::pair<key, val> &operator*() const;
		// 	yeet::pair<key, val> *operator->() const;
		// 	bool operator==(const_iterator it);
		// 	bool operator!=(const_iterator it);
		// 	std::size_t operator-(const_iterator it);
		// };
		template<typename type>	
		struct base_iterator{
			treeit<key, val> iter;
			type *m;

			// base_iterator operator+(unsigned int n);
			// base_iterator operator-(unsigned int n);
			base_iterator operator++(int);
			base_iterator operator--(int);
			yeet::pair<key, val> &operator*() const;
			yeet::pair<key, val> *operator->() const;
			bool operator==(base_iterator it);
			bool operator!=(base_iterator it);
			std::size_t operator-(base_iterator it);
		};

		using iterator = base_iterator<map<key, val, cmp>>;
		using const_iterator = base_iterator<const map<key, val, cmp>>;
		// using const_iterator = const map<key, val, cmp>::base_iterator;

		map(const cmp &comp = std::less<key>{});
		map(yeet::pair<key, val> const *first, yeet::pair<key, val> const *last, const cmp &comp = std::less<key>{});
		map(const map &other);
		map(map &&other);
		~map();

		map& operator=(const map &other);
		map& operator=(map &&other);

		val& at(const key& k);
		const val& at(const key &k) const;

		val& operator[](const key &k);
		val& operator[](key &&k);

		iterator begin();
		const_iterator cbegin() const;
		iterator end();
		const_iterator cend() const;

		bool empty() const;
		std::size_t size() const;
		std::size_t max_size() const;

		void clear();
		yeet::pair<iterator, bool> insert(tree<key, val> value);
		yeet::pair<iterator, bool> insert(const yeet::pair<key, val> &value);
		template<typename inputit>
		void insert(inputit first, inputit last);
		template<class... Args>
		yeet::pair<iterator, bool> emplace(Args&&... args);

		iterator erase(iterator pos);
		iterator erase(const_iterator pos);
		iterator erase(iterator first, iterator last);
		iterator erase(const_iterator first, const_iterator last);
		std::size_t erase(const key &k);

		void swap(map &other);
		template<typename type>
		tree<key, val> extract(base_iterator<type> pos);
		tree<key, val> extract(const key &k);

		template<typename cmp2 = std::less<key>>
		void merge(map<key, val, cmp2> &other);
		std::size_t count(const key &k) const;
		iterator find(const key &k);
		const_iterator find(const key &k) const;
		bool contains(const key &k) const;
		iterator lower_bound(const key &k);
		const_iterator lower_bound(const key &k) const;
		iterator upper_bound(const key &k);
		const_iterator upper_bound(const key &k) const;
		yeet::pair<iterator, iterator> equal_range(const key &k);
		yeet::pair<const_iterator, const_iterator> equal_range(const key &k) const;
		const cmp &key_comp() const;	
	};
}
