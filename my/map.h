#pragma once
#include "map-defs.h"
#include <functional>
#include <utility>

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::map(const cmp &comp) : cmpr(comp){
    this->data = nullptr;
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::map(yeet::pair<key, val> const *first, yeet::pair<key, val> const *last, const cmp &comp) : cmpr(comp){
    this->data = nullptr;
    for (int i = 0; i < last - first; ++i)
    {
        this->insert(first[i]);
    }
}


template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::map(yeet::map<key, val, cmp> const &other)
    : data(clone(other.data))
    , cmpr(other.cmpr)
{}


template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::map(yeet::map<key, val, cmp> &&other) : cmpr(other.cmpr) {
    this->data = other.data;
    other.data = nullptr;
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::~map() {
    destroyTree(this->data);
}

//copy and move operators
template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>& yeet::map<key, val, cmp>::operator=(const map<key, val, cmp> &other) {
    if(this != &other){
        this->~map();
        new (this) map(other);
    }
    return *this;
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>& yeet::map<key, val, cmp>::operator=(yeet::map<key, val, cmp> &&other) {
    if(this != &other){
        this->~map();
        new (this) map(static_cast<yeet::map<key, val, cmp> &&>(other));
    }
    return *this;
}

//methods
template<typename key, typename val, typename cmp>
const val& yeet::map<key, val, cmp>::at(const key& k) const {
    tree<key, val> p = find_node(k, this->data, this->cmpr);
    if(p != nullptr)
        return p->value.v;
    else
        throw std::out_of_range("key not found");
}

template<typename key, typename val, typename cmp>
val& yeet::map<key, val, cmp>::at(const key& k){
    tree<key, val> p = find_node(k, this->data, this->cmpr);
    if(p != nullptr)
        return p->value.v;
    else
        throw std::out_of_range("key not found");
}


template<typename key, typename val, typename cmp>
val& yeet::map<key, val, cmp>::operator[](const key &k){
    tree<key, val>p = find_node(k, this->data, this->cmpr);
    if(p != nullptr)
        return p->value.v;
    else{
        // yeet::pair<key, val> pf = {k, 0};
        val v;
        auto res = insert_tree(k, v, this->data, this->cmpr);
        this->data = balanceup(res.k);
        return res.v->value.v;
    }
}

template<typename key, typename val, typename cmp>
val& yeet::map<key, val, cmp>::operator[](key &&k){
    return (*this)[static_cast<const key &>(k)];
}

template<typename key, typename val, typename cmp>
bool yeet::map<key, val, cmp>::empty() const {
    return this->data == nullptr;
}

template<typename key, typename val, typename cmp>
void yeet::map<key, val, cmp>::clear(){
    destroyTree(this->data);
}

template<typename key, typename val, typename cmp>
yeet::pair<typename yeet::map<key, val, cmp>::iterator, bool> yeet::map<key, val, cmp>::insert(tree<key, val> t){
    tree<key, val> p = find_node(t->value.k, this->data, this->cmpr);
    if(p != nullptr)
        return yeet::pair<yeet::map<key, val, cmp>::iterator, bool>(yeet::map<key, val, cmp>::iterator{treeit{p}, this}, false);
    else{
        auto res = insert_tree(t, this->data, this->cmpr);
        this->data = balanceup(res.k);
        return yeet::pair(yeet::map<key, val, cmp>::iterator{treeit<key, val>{t}, this}, true);
    }
}

template<typename key, typename val, typename cmp>
yeet::pair<typename yeet::map<key, val, cmp>::iterator, bool> yeet::map<key, val, cmp>::insert(const yeet::pair<key, val> &value){
    // std::cout << 0 << std::endl;
    tree<key, val> t = detachednode<key, val>(value);
    // std::cout << 1 << std::endl; 
    return insert(t);
}

template<typename key, typename val, typename cmp> template<typename inputit>
void yeet::map<key, val, cmp>::insert(inputit first, inputit last){
    for(inputit tmp = first; tmp != last; tmp++){
        insert(tmp);
    }
}

template<typename key, typename val, typename cmp> template<class... Args>
yeet::pair<typename yeet::map<key, val, cmp>::iterator, bool> yeet::map<key, val, cmp>::emplace(Args&&... args){
    // yeet::pair<const key&, const val&> invalue(static_cast<Args&&>(args)...);
    tree<key, val> t = detachednode<key, val>(static_cast<Args&&>(args)...);
    tree<key, val> p = find_node(t->value.k, this->data, this->cmpr);
    if(p != nullptr)
        return yeet::pair<yeet::map<key, val, cmp>::iterator, bool>(yeet::map<key, val, cmp>::iterator{treeit{p}, this}, false);
    else{
        auto res = insert_tree<key, val>(t, this->data, this->cmpr);
        this->data = balanceup(res.k);
        return yeet::pair(yeet::map<key, val, cmp>::iterator{treeit<key, val>{res.v}, this}, true);
    }
}


//iterator
template<typename key, typename val, typename cmp, typename type>
yeet::map<key, val, cmp>::base_iterator<type> last(type *m){
    treeit<key, val> raw = {m->data};
    while (raw.base->right != nullptr) {
        raw.base = raw.base->right;
    }
    return {raw, m};
}

template<typename key, typename val, typename cmp> template<typename type>
yeet::map<key, val, cmp>::base_iterator<type> yeet::map<key, val, cmp>::base_iterator<type>::operator++(int) {
    if(this->iter.base == nullptr)
        return *this;
    if(*this != last<key, val, cmp, type>(this->m))
        this->iter++;
    else
        this->iter.base = nullptr;
    return *this;
}

template<typename key, typename val, typename cmp> template<typename type>
yeet::map<key, val, cmp>::base_iterator<type> yeet::map<key, val, cmp>::base_iterator<type>::operator--(int) {
    if(this->iter.base != nullptr)
        this->iter--;
    else
        this->iter = last<key, val, cmp, type>(this->m).iter;
    return *this;
}

// template<typename key, typename val, typename cmp> template<typename type>
// typename yeet::map<key, val, cmp>::base_iterator<type> yeet::map<key, val, cmp>::base_iterator<type>::operator+(unsigned int n) {
//     yeet::map<key, val, cmp>::base_iterator<type> tmp = *this;
//     for (size_t i = 0; i < n; i++) {
//         tmp++;
//     }
//     return tmp;
// }

// template<typename key, typename val, typename cmp> template<typename type>
// typename yeet::map<key, val, cmp>::base_iterator<type> yeet::map<key, val, cmp>::base_iterator<type>::operator-(unsigned int n) {
//     yeet::map<key, val, cmp>::base_iterator<type> tmp = *this;
//     for (size_t i = 0; i < n; i++) {
//         tmp--;
//     }
//     return tmp;
// }

template<typename key, typename val, typename cmp> template<typename type>
yeet::pair<key, val>& yeet::map<key, val, cmp>::base_iterator<type>::operator*() const {
    return this->iter.base->value;
}

template<typename key, typename val, typename cmp> template<typename type>
yeet::pair<key, val>* yeet::map<key, val, cmp>::base_iterator<type>::operator->() const {
    return &(this->iter.base->value);
}

template<typename key, typename val, typename cmp> template<typename type>
bool yeet::map<key, val, cmp>::base_iterator<type>::operator==(yeet::map<key, val, cmp>::base_iterator<type> it){
    return this->iter.base == it.iter.base;
}

template<typename key, typename val, typename cmp> template<typename type>
bool yeet::map<key, val, cmp>::base_iterator<type>::operator!=(yeet::map<key, val, cmp>::base_iterator<type> it){
    return !(*this == it);
}

template<typename key, typename val, typename cmp> template<typename type>
std::size_t yeet::map<key, val, cmp>::base_iterator<type>::operator-(base_iterator<type> it){
    std::size_t counter = 0;
    // std::cout << "+1" << std::endl;
    while(it != *this) 
    {
        it++;
        counter++;
    }
    return counter;
}

//iterator based functions
template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::begin(){
    if(this->data == nullptr)
        return {{nullptr}, this};
    if(this->data->left == nullptr)
        return {{this->data}, this};
    treeit<key, val> raw = {this->data};
    while(decr(raw).base != this->data) {
        raw--;
    }
    return {raw, this};
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::const_iterator yeet::map<key, val, cmp>::cbegin() const {
    if(this->data == nullptr)
        return const_iterator{{nullptr}, this};
    if(this->data->left == nullptr)
        return const_iterator{{this->data}, this};
    treeit<key, val> raw = {this->data};
    while (decr(raw).base != this->data) {
        raw--;
    }
    return {raw, this};
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::end(){
    treeit<key, val> raw = {nullptr};
    return {raw, this};
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::const_iterator yeet::map<key, val, cmp>::cend() const {
    treeit<key, val> raw = {nullptr};
    return {raw, this};
}

template<typename key, typename val, typename cmp>
std::size_t yeet::map<key, val, cmp>::size() const {
    return this->cend() - this->cbegin();
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::erase(yeet::map<key, val, cmp>::iterator it){
    yeet::map<key, val, cmp>::iterator ret = it;
    ret++;
    this->data = balanceup(remove(it.iter.base).k);
    return ret;
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::erase(yeet::map<key, val, cmp>::const_iterator it){
    yeet::map<key, val, cmp>::iterator ret = it;
    ret++;
    this->data = balanceup(remove(it.iter.base).k);
    return ret;
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::erase(yeet::map<key, val, cmp>::iterator first, yeet::map<key, val, cmp>::iterator last){
    yeet::map<key, val, cmp>::iterator ret = first;
    // int n = last - first;
    for (; ret != last; ret++)
    {
        ret = erase(ret);
    }
    return ret;
}

template<typename key, typename val, typename cmp>
typename yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::erase(yeet::map<key, val, cmp>::const_iterator first, yeet::map<key, val, cmp>::const_iterator last){
    yeet::map<key, val, cmp>::const_iterator ret = first;
    // int n = last - first;
    for (; ret != last; ret++)
    {
        std::cout << ret->v << std::endl;
        ret = erase(ret);
    }
    return ret;
}

template<typename key, typename val, typename cmp>
std::size_t yeet::map<key, val, cmp>::erase(const key &k){
    tree<key, val> t = find_node(k, this->data, this->cmpr);
    if(t == nullptr)
        return 0;
    else{
        erase(yeet::map<key, val, cmp>::iterator{treeit<key, val>{t}, this});
        return 1;
    }
}

template<typename key, typename val, typename cmp>
void yeet::map<key, val, cmp>::swap(yeet::map<key, val, cmp> &other){
    tree<key, val> tmp = this->data;
    this->data = other.data;
    other.data = tmp;
}

template<typename key, typename val, typename cmp> template<typename type>
tree<key, val> yeet::map<key, val, cmp>::extract(base_iterator<type> pos){
    yeet::pair<tree<key, val>, tree<key, val>> ret = remove(pos.iter.base, false);
    ret.k = balanceup(ret.k);
    this->data = ret.k;
    ret.v->left = nullptr;
    ret.v->right = nullptr;
    ret.v->parent = nullptr;
    return ret.v;
}

template<typename key, typename val, typename cmp>
tree<key, val> yeet::map<key, val, cmp>::extract(const key &k){
    tree<key, val> rmval = find_node(k, this->data, this->cmpr);
    yeet::pair<tree<key, val>, tree<key, val>> ret = remove(rmval, false);
    ret.k = balanceup(ret.k);
    this->data = ret.k;
    ret.v->left = nullptr;
    ret.v->right = nullptr;
    ret.v->parent = nullptr;
    return ret.v;
}

template<typename key, typename val, typename cmp> template<typename cmp2>
void yeet::map<key, val, cmp>::merge(yeet::map<key, val, cmp2> &other){
    unsigned int len = static_cast<unsigned int>(other.size());
    for (unsigned int i = 0; i < len; ++i)
    {
        typename yeet::map<key, val, cmp2>::const_iterator tmp = other.cbegin();
        tree<key, val> t = other.extract(tmp);
        if(!this->insert(t).v)
            destroyTree(t);
    }
}

template<typename key, typename val, typename cmp>
std::size_t yeet::map<key, val, cmp>::count(const key &k) const{
    tree<key, val> t = find_node(k, this->data, this->cmpr);
    if(t == nullptr)
        return 0;
    return 1;
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::find(const key &k){
    return {treeit{find_node(k, this->data, this->cmpr)}, this};
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::const_iterator yeet::map<key, val, cmp>::find(const key &k) const{
    return {treeit{find_node(k, this->data, this->cmpr)}, this};
}

template<typename key, typename val, typename cmp>
bool yeet::map<key, val, cmp>::contains(const key &k) const{
    return (find_node(k, this->data, this->cmpr) == nullptr) ? 0 : 1;
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::lower_bound(const key &k){
    return {treeit{find_node(k, this->data, this->cmpr, 1)}, this};
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::iterator yeet::map<key, val, cmp>::upper_bound(const key &k){
    yeet::map<key, val, cmp>::iterator it = lower_bound(k);
    if(*it.k == k)
        return it + 1;
    return it;
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::const_iterator yeet::map<key, val, cmp>::lower_bound(const key &k) const{
    return {treeit{find_node(k, this->data, this->cmpr, 1)}, this};
}

template<typename key, typename val, typename cmp>
yeet::map<key, val, cmp>::const_iterator yeet::map<key, val, cmp>::upper_bound(const key &k) const{
    yeet::map<key, val, cmp>::iterator it = lower_bound(k);
    if(*it.k == k)
        return it + 1;
    return it;
}

template<typename key, typename val, typename cmp>
yeet::pair<typename yeet::map<key, val, cmp>::iterator, typename yeet::map<key, val, cmp>::iterator> yeet::map<key, val, cmp>::equal_range(const key &k){
    return {lower_bound(k), upper_bound(k)};
}

template<typename key, typename val, typename cmp>
yeet::pair<typename yeet::map<key, val, cmp>::const_iterator, typename yeet::map<key, val, cmp>::const_iterator> yeet::map<key, val, cmp>::equal_range(const key &k) const{
    return {lower_bound(k), upper_bound(k)};
}

template<typename key, typename val, typename cmp>
const cmp &yeet::map<key, val, cmp>::key_comp() const{
    return this->cmpr;
}

template<typename key, typename val, typename cmp>
bool operator==(const yeet::map<key, val, cmp> &lhs, const yeet::map<key, val, cmp> &rhs){
    typename yeet::map<key, val, cmp>::const_iterator lit = lhs.cbegin();
    typename yeet::map<key, val, cmp>::const_iterator rit = rhs.cbegin();
    for (unsigned int i = 0; i < lhs.size(); ++i)
    {
        if(!(lit.iter.base == nullptr) != !(rit.iter.base == nullptr))
            return 0;
        if(lit.iter.base == nullptr && rit.iter.base == nullptr)
            return 1;
        if(lit->k != rit->k || lit->v != rit->v)
            return 0;
        lit++;
        rit++;
    }
    return 1;
}
