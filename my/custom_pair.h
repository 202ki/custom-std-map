#include <iostream>

// bool condition = true;

namespace yeet{

  template<typename key, typename val>
  struct pair {
    key k;
    val v;
  };

template<typename key, typename val>
std::ostream &operator<<(std::ostream &out, pair<key, val> p){
  out << p.k << " " << p.v;
  return out;
}


}
